var path = require('path');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.devDS;
ds.automigrate('Product', function(err) {
  if (err) throw err;

  var products = [
    {
      code: '1234',
      category_name: 'Говядина',
      name: 'Тушка',
      branch: 'Teameat по Сейфуллина',
      createdAt: new Date(),
      amount: 24
    }
  ];
  var count = products.length;
  products.forEach(function(product) {
    app.models.Product.create(product, function(err, model) {
      if (err) throw err;

      console.log('Created:', model);

      count--;
      if (count === 0)
        ds.disconnect();
    });
  });
});